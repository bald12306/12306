#ifndef LJQ_MAP_HPP
#define LJQ_MAP_HPP
#define H 10
#define ME (*this)

#include<cstring>
#include <unistd.h>

namespace wyll {

template<int multi, class Key, class T, class Compare = std::less<Key> > 
class map {
    public:
	static const int MM = (4150 / (sizeof(Key) + 8) - 2) >> 1; 
	static const int LL = ((4196 / sizeof(T) - 1) >> 1)?(4196 / sizeof(T) - 1):1; 
	int M, L; 
	char Name[3]; 
	FILE* srcf; 
	FILE* datf; 
	struct TT{
	    T key[(LL << 1) + 1]; 
	}; 
	struct NODE{
	    bool leaf; 
	    size_t dat, loc; 
	    int sz; 
	    size_t lb, rb; 
	    size_t son[(MM << 1) + 2]; 
	    Key key[(MM << 1) + 1]; 
	}; 
	size_t root; 
	size_t srccnt, datcnt; 
	template < class TTT > 
	void write(TTT& dat, size_t loc, FILE*& file){
	    fseek(file, loc, SEEK_SET); 
	    fwrite(&dat, sizeof(TTT), 1, file); 
	    fflush(file); 
	}
	template < class TTT > 
	void read(TTT& dat, size_t loc, FILE*& file){
	    fseek(file, loc, SEEK_SET); 
	    fread(&dat, sizeof(TTT), 1, file); 
	}
	void init(){
	    root = sizeof(NODE); 
	    write(root, 0, srcf); 
	    srccnt = sizeof(NODE) << 1; 
	    write(srccnt, sizeof(size_t), srcf); 
	    datcnt = sizeof(TT); 
	    write(datcnt, sizeof(size_t) << 1, srcf); 
	    NODE tmp; 
	    tmp.sz = 0; 
	    tmp.loc = root; 
	    tmp.lb = tmp.rb = 0; 
	    tmp.leaf = true; 
	    tmp.dat = 0; 
	    write(tmp, tmp.loc, srcf); 
	    TT tt; 
	    write(tt, tmp.dat, datf); 
	}
	int getupp(const NODE& nd, const Key& key){
	    int l = -1; int r = nd.sz; 
	    while (r - l - 1){
		int mid = (l + r) >> 1; 
		if (Compare()(key, nd.key[mid]))    r = mid; 
		else	l = mid; 
	    }
	    return r; 
	}
	int getlow(const NODE& nd, const Key& key){
	    int l = -1; int r = nd.sz; 
	    while (r - l - 1){
		int mid = (l + r) >> 1; 
		if (Compare()(key, nd.key[mid]) || key == nd.key[mid])	r = mid; 
		else	l = mid; 
	    }
	    return r; 
	}
    public:
	class iterator {
	    private:	
		FILE* srcf; 
		FILE* datf; 
		template < class TTT > 
		void read(TTT& dat, size_t loc, FILE*& file){
		    fseek(file, loc, SEEK_SET); 
		    fread(&dat, sizeof(TTT), 1, file); 
		}
		template < class TTT > 
		void write(TTT& dat, size_t loc, FILE*& file){
		    fseek(file, loc, SEEK_SET); 
		    fwrite(&dat, sizeof(TTT), 1, file); 
		    fflush(file); 
		}
	    public:
		size_t rb; 
		size_t dat; 
		int sz, w; 
		iterator(){}
		iterator(FILE*& _srcf, FILE*& _datf) {
		    srcf = _srcf; 
		    datf = _datf; 
		}
		iterator(const iterator &other) {
		    srcf = other.srcf; 
		    datf = other.datf; 
		    rb = other.rb; 
		    sz = other.sz; 
		    dat = other.dat; 
		    w = other.w; 
		}
		iterator operator++(int) {
		    iterator res = ME; 
		    if (++w == sz){
			if (rb == 0)	return res; 
			NODE tmp; 
			read(tmp, rb, srcf); 
			sz = tmp.sz; 
			rb = tmp.rb; 
			dat = tmp.dat; 
			w = 0; 
		    }
		    return res; 
		}
		iterator & operator++() {
		    if (++w == sz){
			if (rb == 0)	return ME; 
			NODE tmp; 
			read(tmp, rb, srcf); 
			sz = tmp.sz; 
			rb = tmp.rb; 
			dat = tmp.dat; 
			w = 0; 
		    }
		    return ME; 
		}
		T operator*() {
		    TT tt; 
		    read(tt, dat, datf); 
		    return tt.key[w]; 
		}
		void edit(const T& t){
		    TT tt; 
		    read(tt, dat, datf); 
		    tt.key[w] = t; 
		    write(tt, dat, datf); 
		}
		bool operator==(const iterator &rhs) const {
		    return (dat == rhs.dat && w == rhs.w); 
		}
		bool operator!=(const iterator &rhs) const {
		    return !(ME == rhs); 
		}
	};
	map(const char* _Name){
	    M = MM; 
	    if (MM < LL)    L = MM; 
	    else    L = LL; 
	    memcpy(Name, _Name, 3); 
	    char* file = new char[10]; 
	    memcpy(file, Name, 3); 
	    file[3] = '_', file[4] = 's', file[5] = '.',  file[6] = 'b'; file[7] = 'i'; file[8] = 'n'; file[9] = 0; 
	    if (access(file, 0) == -1){
		srcf = fopen(file, "wb+"); 
		file[4] = 'd'; 
		datf = fopen(file, "wb+"); 
	    	init(); 
	    }else{
		srcf = fopen(file, "rb+"); 
		file[4] = 'd'; 
		datf = fopen(file, "rb+"); 
		read(root, 0, srcf); 
		read(srccnt, sizeof(size_t), srcf); 
		read(datcnt, sizeof(size_t) << 1, srcf); 
	    }
	    delete[] file; 
	}
	~map(){
	    fclose(srcf); 
	    fclose(datf); 
	}
	void insert(const Key &key, const T &t) {
	    if (multi == 0 && exist(key)){
	       	edit(key, t); 
		return; 
	    }
	    NODE nd; 
	    NODE f[H]; 
	    int ws[H]; 
	    int h = 0; 
	    read(nd, root, srcf); 
	    while (!nd.leaf){
		int i = getupp(nd, key); 
		f[h] = nd; 
		ws[h ++] = i; 
		read(nd, nd.son[i], srcf); 
	    }
	    int i = getupp(nd, key); 
	    for (int j = nd.sz; j > i; j--) nd.key[j] = nd.key[j - 1]; 
	    nd.key[i] = key; 
	    nd.sz++; 
	    write(nd, nd.loc, srcf); 
	    TT tt; 
	    read(tt, nd.dat, datf); 
	    for (int j = nd.sz - 1; j > i; j--)	tt.key[j] = tt.key[j - 1]; 
	    tt.key[i] = t; 
	    write(tt, nd.dat, datf); 
	    if (nd.sz == (L << 1)){
		NODE tmpn; 
		TT tmpt; 
		tmpn.dat = datcnt; 
		datcnt += sizeof(TT); 
		write(datcnt, sizeof(size_t) << 1, srcf); 
		for (int i = 0; i < L; i++) tmpt.key[i] = tt.key[L + i]; 
		write(tmpt, tmpn.dat, datf); 
		tmpn.leaf = nd.leaf; 
		tmpn.sz = L; 
		tmpn.loc = srccnt; 
		srccnt += sizeof(NODE); 
		write(srccnt, sizeof(size_t), srcf); 
		tmpn.lb = nd.loc; 
		tmpn.rb = nd.rb; 
		for (int i = 0; i < L; i++) tmpn.key[i] = nd.key[L + i]; 
		write(tmpn, tmpn.loc, srcf); 
		nd.sz = L; 
		nd.rb = tmpn.loc; 
		write(nd, nd.loc, srcf); 
		Key mid = tmpn.key[0]; 
		if (nd.loc == root){
		    root = srccnt; 
		    write(root, 0, srcf); 
		    srccnt += sizeof(NODE); 
		    write(srccnt, sizeof(size_t), srcf); 
		    NODE tmp; 
		    tmp.leaf = false; 
		    tmp.sz = 1; 
		    tmp.key[0] = mid; 
		    tmp.loc = root; 
		    tmp.lb = tmp.rb = 0; 
		    tmp.son[0] = nd.loc; 
		    tmp.son[1] = tmpn.loc; 
		    write(tmp, tmp.loc, srcf); 
		    write(tmpn, tmpn.loc, srcf); 
		    write(nd, nd.loc, srcf); 
		}else{
		    nd = f[--h]; 
		    int i = ws[h]; 
		    for (int j = nd.sz; j > i; j --){
			nd.key[j] = nd.key[j - 1]; 
			nd.son[j + 1] = nd.son[j]; 
		    }
		    nd.key[i] = mid; 
		    nd.son[i + 1] = tmpn.loc; 
		    nd.sz++; 
		    write(nd, nd.loc, srcf); 
		}
	    }
	    while (nd.sz == (M << 1) + 1){
		NODE tmpn; 
		tmpn.leaf = nd.leaf; 
		tmpn.sz = M; 
		tmpn.loc = srccnt; 
		srccnt += sizeof(NODE); 
		write(srccnt, sizeof(size_t), srcf); 
		tmpn.lb = nd.loc; 
		tmpn.rb = nd.rb; 
		for (int i = 0; i < M; i++) tmpn.key[i] = nd.key[M + i + 1]; 
		for (int i = 0; i < M + 1; i++)	tmpn.son[i] = nd.son[M + i + 1]; 
		write(tmpn, tmpn.loc, srcf); 
		nd.sz = M; 
		nd.rb = tmpn.loc; 
		write(nd, nd.loc, srcf); 
		Key mid = nd.key[M]; 
		if (nd.loc == root){
		    root = srccnt; 
		    write(root, 0, srcf); 
		    srccnt += sizeof(NODE); 
		    write(srccnt, sizeof(size_t), srcf); 
		    NODE tmp; 
		    tmp.leaf = false; 
		    tmp.sz = 1; 
		    tmp.key[0] = mid; 
		    tmp.loc = root; 
		    tmp.lb = tmp.rb = 0; 
		    tmp.son[0] = nd.loc; 
		    tmp.son[1] = tmpn.loc; 
		    write(tmp, tmp.loc, srcf); 
		    write(nd, nd.loc, srcf); 
		}else{
		    nd = f[--h]; 
		    int i = ws[h]; 
		    for (int j = nd.sz; j > i; j --){
			nd.key[j] = nd.key[j - 1]; 
			nd.son[j + 1] = nd.son[j]; 
		    }
		    nd.key[i] = mid; 
		    nd.son[i + 1] = tmpn.loc; 
		    nd.sz++; 
		    write(nd, nd.loc, srcf); 
		}
	    }
	}
	const T operator[](const Key &key) {
	    iterator res = lowerbound(key); 
	    return *res; 
	}
	iterator lowerbound(const Key key) {
	    NODE nd; 
	    read(nd, root, srcf); 
	    while (!nd.leaf){
		int i = getlow(nd, key); 
		read(nd, nd.son[i], srcf); 
	    }
	    iterator res(srcf, datf); 
	    res.rb = nd.rb; 
	    res.sz = nd.sz; 
	    res.dat = nd.dat; 
	    int i = getlow(nd, key); 
	    if (i == nd.sz && nd.rb){
		res.w = i - 1; 
		res ++; 
	    }else   res.w = i; 
	    return res; 
	}
	iterator upperbound(const Key key) {
	    NODE nd; 
	    read(nd, root, srcf); 
	    while (!nd.leaf){
		int i = getupp(nd, key); 
		read(nd, nd.son[i], srcf); 
	    }
	    iterator res(srcf, datf); 
	    res.rb = nd.rb; 
	    res.sz = nd.sz; 
	    res.dat = nd.dat; 
	    int i = getupp(nd, key); 
	    if (i == nd.sz && nd.rb){
		res.w = i - 1; 
		res ++; 
	    }else   res.w = i; 
	    return res; 
	}
	void clear() {	
	    fclose(srcf); 
	    fclose(datf); 
	    char* file = new char[10]; 
	    memcpy(file, Name, 3); 
	    file[3] = '_', file[4] = 's', file[5] = '.',  file[6] = 'b'; file[7] = 'i'; file[8] = 'n'; file[9] = 0; 
	    srcf = fopen(file, "wb+"); 
	    file[4] = 'd'; 
	    datf = fopen(file, "wb+"); 
	    init(); 
	}
	void erase(Key key) {
	    if (multi)	return; 
	    NODE nd; 
	    NODE fa[H]; 
	    int ws[H]; 
	    int h = 0; 
	    read(nd, root, srcf); 
	    while (!nd.leaf){
		int i = getupp(nd, key); 
		fa[h] = nd; 
		ws[h ++] = i; 
		read(nd, nd.son[i], srcf); 
	    }
	    int i = getlow(nd, key); 
	    if (nd.key[i] != key) return; 
	    TT tt; 
	    read(tt, nd.dat, datf); 
	    for (int j = i; j < nd.sz - 1; j++){
		nd.key[j] = nd.key[j + 1]; 
		tt.key[j] = tt.key[j + 1]; 
	    }
	    nd.sz--; 
	    write(nd, nd.loc, srcf); 
	    write(tt, nd.dat, datf); 
	    if (nd.loc == root)	return; 
	    if (nd.sz < L){
		if (nd.lb){
		    NODE lb; 
		    read(lb, nd.lb, srcf); 
		    if (lb.sz > L){
			TT tt, ttlb; 
			read(tt, nd.dat, datf); 
			read(ttlb, lb.dat, datf); 
			for (int i = nd.sz; i; i--) tt.key[i] = tt.key[i - 1]; 
			tt.key[0] = ttlb.key[lb.sz - 1]; 
			for (int i = nd.sz; i; i--) nd.key[i] = nd.key[i - 1]; 
			nd.key[0] = lb.key[lb.sz - 1]; 
			nd.sz++; lb.sz--; 
			NODE f; 
			f = fa[h - 1]; 
			int i = ws[h - 1] - 1; 
			f.key[i] = nd.key[0]; 
			write(tt, nd.dat, datf); 
			write(ttlb, lb.dat, datf); 
			write(nd, nd.loc, srcf); 
			write(lb, lb.loc, srcf); 
			write(f, f.loc, srcf); 
		    }
		}
	    }
	    if (nd.sz < L){
		if (nd.rb){
		    NODE rb; 
		    read(rb, nd.rb, srcf); 
		    if (rb.sz > L){
			TT tt, ttrb; 
			read(tt, nd.dat, datf); 
			read(ttrb, rb.dat, datf); 
			tt.key[nd.sz] = ttrb.key[0]; 
			for (int i = 0; i < rb.sz; i++) ttrb.key[i] = ttrb.key[i + 1]; 
			nd.key[nd.sz] = rb.key[0]; 
			for (int i = 0; i < rb.sz; i++) rb.key[i] = rb.key[i + 1]; 
			nd.sz++; rb.sz--; 
			NODE f; 
			f = fa[h - 1]; 
			int i = ws[h - 1]; 
			f.key[i] = rb.key[0]; 
			write(tt, nd.dat, datf); 
			write(ttrb, rb.dat, datf); 
			write(nd, nd.loc, srcf); 
			write(rb, rb.loc, srcf); 
			write(f, f.loc, srcf); 
		    }
		}
	    }
	    if (nd.sz < L){
		if (nd.lb){
		    NODE lb; 
		    read(lb, nd.lb, srcf); 
		    lb.rb = nd.rb; 
		    if (nd.rb){
			NODE rb; 
			read(rb, nd.rb, srcf); 
			rb.lb = lb.loc; 
		    }
		    NODE f; 
		    f = fa[h - 1]; 
		    int i = ws[h - 1] - 1; 
		    for (int j = i; j < f.sz; j++)  f.key[j] = f.key[j + 1]; 
		    for (int j = i + 1; j < f.sz + 1; j++) f.son[j] = f.son[j + 1]; 
		    f.sz--; 
		    for (int j = 0; j < nd.sz; j++) lb.key[j + lb.sz] = nd.key[j]; 
		    TT tt, ttlb; 
		    read(tt, nd.dat, datf); 
		    read(ttlb, lb.dat, datf); 
		    for (int j = 0; j < nd.sz; j++) ttlb.key[j + lb.sz] = tt.key[j]; 
		    lb.sz += nd.sz; 
		    write(ttlb, lb.dat, datf); 
		    write(lb, lb.loc, srcf); 
		    write(f, f.loc, srcf); 
		}else{
		    NODE rb; 
		    read(rb, nd.rb, srcf); 
		    nd.rb = rb.rb; 
		    if (rb.rb){
			NODE rrb; 
			read(rrb, rb.rb, srcf); 
			rrb.lb = nd.loc; 
		    }
		    NODE f; 
		    f = fa[h - 1]; 
		    int i = ws[h - 1]; 
		    for (int j = i; j < f.sz; j++)  f.key[j] = f.key[j + 1]; 
		    for (int j = i + 1; j < f.sz + 1; j++) f.son[j] = f.son[j + 1]; 
		    f.sz--; 
		    for (int j = 0; j < rb.sz; j++) nd.key[j + nd.sz] = rb.key[j]; 
		    TT tt, ttrb; 
		    read(tt, nd.loc, datf); 
		    read(ttrb, rb.loc, datf); 
		    for (int j = 0; j < rb.sz; j++) tt.key[j + nd.sz] = ttrb.key[j]; 
		    nd.sz += rb.sz; 
		    write(tt, nd.dat, datf); 
		    write(nd, nd.loc, srcf); 
		    write(f, f.loc, srcf); 
		}
	    }
	    nd = fa[--h]; 
	    while (nd.loc != root && nd.sz < M){
		if (nd.sz < M){
		    if (nd.lb){
			NODE lb; 
			read(lb, nd.lb, srcf); 
			if (lb.sz > M){
			    NODE f; 
			    f = fa[h - 1]; 
			    int i = ws[h - 1] - 1; 
			    for (int j = nd.sz; j; j--) nd.key[j] = nd.key[j - 1]; 
			    for (int j = nd.sz + 1; j; j--) nd.son[j] = nd.son[j - 1]; 
			    nd.key[0] = f.key[i]; 
			    nd.son[0] = lb.son[lb.sz]; 
			    f.key[i] = lb.key[lb.sz - 1]; 
			    nd.sz++; lb.sz--; 
			    write(nd, nd.loc, srcf); 
			    write(lb, lb.loc, srcf); 
			    write(f, f.loc, srcf); 
			}
		    }
		}
		if (nd.sz < M){
		    if (nd.rb){
			NODE rb; 
			read(rb, nd.rb, srcf); 
			if (rb.sz > L){
			    NODE f; 
			    f = fa[h - 1]; 
			    int i = ws[h - 1]; 
			    nd.key[nd.sz] = f.key[i]; 
			    nd.son[nd.sz + 1] = rb.son[0]; 
			    f.key[i] = rb.key[0]; 
			    for (int j = 0; j < rb.sz; j++) rb.key[j] = nd.key[j + 1]; 
			    for (int j = 0; j < rb.sz + 1; j++) rb.son[j] = nd.son[j + 1]; 
			    nd.sz++; rb.sz--; 
			    write(nd, nd.loc, srcf); 
			    write(rb, rb.loc, srcf); 
			    write(f, f.loc, srcf); 
			}
		    }
		}
		if (nd.sz < M){
		    if (nd.lb){
			NODE lb; 
			read(lb, nd.lb, srcf); 
			lb.rb = nd.rb; 
			if (nd.rb){
			    NODE rb; 
			    read(rb, nd.rb, srcf); 
			    rb.lb = lb.loc; 
			}
			NODE f; 
			f = fa[h - 1]; 
			int i = ws[h - 1] - 1; 
			lb.key[lb.sz++] = f.key[i]; 
			for (int j = i; j < f.sz - 1; j++)  f.key[j] = f.key[j + 1]; 
			for (int j = i + 1; j < f.sz + 1; j++)	f.son[j] = f.son[j + 1]; 
			f.sz--; 
			for (int j = 0; j < nd.sz; j++) lb.key[j + lb.sz] = nd.key[j]; 
			lb.sz += nd.sz; 
			write(lb, lb.loc, srcf); 
			write(f, f.loc, srcf); 
		    }else{
			NODE rb; 
			read(rb, nd.rb, srcf); 
			nd.rb = rb.rb; 
			if (rb.rb){
			    NODE rrb; 
			    read(rrb, rb.rb, srcf); 
			    rrb.lb = nd.loc; 
			}
			NODE f; 
			f = fa[h - 1]; 
			int i = ws[h - 1]; 
			nd.key[nd.sz++] = f.key[i]; 
			for (int j = i; j < f.sz - 1; j++)  f.key[j] = f.key[j + 1]; 
			for (int j = i + 1; j < f.sz + 1; j++)	f.son[j] = f.son[j + 1]; 
			f.sz--; 
			for (int j = 0; j < rb.sz; j++) nd.key[j + nd.sz] = rb.key[j]; 
			nd.sz += rb.sz; 
			write(nd, nd.loc, srcf); 
			write(f, f.loc, srcf); 
		    }
		}	
		nd = fa[--h]; 
	    }
	    if (nd.loc == root && nd.sz == 0){
		root = nd.son[0]; 
		write(root, 0, srcf); 
	    }
	}
	void edit(const Key &key, const T &t){
	    NODE nd; 
	    read(nd, root, srcf); 
	    while (!nd.leaf){
		int i = getupp(nd, key); 
		read(nd, nd.son[i], srcf); 
	    }
	    TT tt; 
	    read(tt, nd.dat, datf); 
	    int i = getlow(nd, key); 
	    tt.key[i] = t; 
	    write(tt, nd.dat, datf); 
	}
	bool exist(const Key &key) {
	    NODE nd; 
	    read(nd, root, srcf); 
	    while (!nd.leaf){
		int i = getupp(nd, key); 
		read(nd, nd.son[i], srcf); 
	    }  
	    int i = getlow(nd, key); 
		if (nd.sz == i)	return false;
	    return nd.key[i] == key; 
	}
};

}

#endif
