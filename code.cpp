#include <cstdio>
#include <iostream>
#include "class.h"
//#include "exception.h"
using namespace wyll;

void(*function[17])() = { register_, login, query_profile, modify_profile, modify_privilege,
						  add_train, sale_train, modify_train, query_train, delete_train,
						  query_ticket, query_transfer, buy_ticket, query_order, refund_ticket,
						  clean, exit };
char name[40][17] = { "register", "login", "query_profile", "modify_profile", "modify_privilege",
					"add_train", "sale_train", "modify_train", "query_train", "delete_train",
					"query_ticket", "query_transfer", "buy_ticket", "query_order", "refund_ticket",
					"clean", "exit" };
					
int main()
{
	char s[100];
	while (!std::cin.eof())
	{
		std::cin >> s;
		for (int i = 0; i < 17; i++)
		{
			if (strcmp(name[i], s) == 0)
			{
				function[i]();
				if (i == 16)
				{
					system("Pause");
					return 0;
				}
				break;
			}
		}
	}
	system("Pause");
	return 0;
}
