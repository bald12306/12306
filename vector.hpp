#ifndef LJQ_VECTOR_HPP
#define LJQ_VECTOR_HPP
#define ME (*this)

#include<cstring>
#include <unistd.h>

namespace wyll{

template<class Key, int init = 1>
class vector{
    private:
	char Name[3]; 
	FILE* vecf; 
	struct NODE{
	    Key key; 
	    bool del; 
	}; 
	size_t sz; 
	template < class TTT > 
	void write(TTT& dat, size_t loc){
	    fseek(vecf, loc, SEEK_SET); 
	    fwrite(&dat, sizeof(TTT), 1, vecf); 
	    fflush(vecf); 
	}
	template < class TTT > 
	void read(TTT& dat, size_t loc){
	    fseek(vecf, loc, SEEK_SET); 
	    fread(&dat, sizeof(TTT), 1, vecf); 
	}
    public:
	vector(const char* _Name){
	    memcpy(Name, _Name, 3); 
	    char* file = new char[10]; 
	    memcpy(file, Name, 3); 
	    file[3] = '_', file[4] = 'v', file[5] = '.',  file[6] = 'b'; file[7] = 'i'; file[8] = 'n'; file[9] = 0; 
	    if (access(file, 0) == -1){
		vecf = fopen(file, "wb+"); 
		sz = 0; 
		write(sz, 0); 
		if (init == 0)	push_back(Key()); 
	    }else{
		vecf = fopen(file, "rb+"); 
		read(sz, 0); 
	    }
	    delete[] file; 
	}
	void clear(){
	    fclose(vecf); 
	    char* file = new char[10]; 
	    memcpy(file, Name, 3); 
	    file[3] = '_', file[4] = 'v', file[5] = '.', file[6] = 'b', file[7] = 'i', file[8] = 'n'; file[9] = 0; 
	    vecf = fopen(file, "wb+"); 
	    sz = 0; 
	    write(sz, 0); 
	    if (init == 0)  push_back(Key()); 
	}
	~vector(){
	    fclose(vecf); 
	}
	size_t push_back(const Key& key){
	    NODE tmp; 
	    tmp.key = key; 
	    tmp.del = false; 
	    sz++; 
		write(sz, 0); 
	    write(tmp, sz * sizeof(NODE)); 
	    return sz - 1; 
	}
	void edit(size_t loc, Key key){
	    NODE tmp; 
	    tmp.key = key; 
	    tmp.del = false; 
	    write(tmp, (loc + 1) * sizeof(NODE)); 
	}
	const Key operator[](const int loc){
	    NODE tmp; 
	    read(tmp, (loc + 1) * sizeof(NODE)); 
	    return tmp.key; 
	}
	void erase(size_t loc){
	    NODE tmp; 
	    read(tmp, (loc + 1) * sizeof(NODE)); 
	    tmp.del = true; 
	    write(tmp, (loc + 1) * sizeof(NODE)); 
	}
	size_t size(){
	    return sz; 
	}
}; 

}

#endif
