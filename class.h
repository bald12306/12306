//#pragma once
#include <cstdio>
#include <cstring>
#include <iostream>
#include <iomanip>
#include "vector.hpp"
#include "map.hpp"
namespace wyll
{
	double price_to_double(char* s)
	{
		int i = 0;
		while (s[i] > '9' || s[i] < '0')i++;
		double ans = 0;
		while (s[i] != '.')
		{
			ans = ans * 10 + (s[i] - '0');
			i++;
		}
		while (s[i] != '\0')i++;
		i--;
		double dot = 0;
		while (s[i] != '.')
		{
			dot = dot * 0.1 + (s[i] - '0');
			i--;
		}
		dot *= 0.1;
		ans += dot;
		return ans;
	}
	class string
	{
	public:
		char storage[50];
		size_t len;
		string() :len() {}
		string(const char* s)
		{
			len = strlen(s);
			strncpy(storage, s, len);
		}
		string& operator=(const string& other)
		{
			len = other.len;
			strncpy(storage, other.storage, len);
			return *this;
		}
		string(const string& other)
		{
			len = other.len;
			strncpy(storage, other.storage, len);
		}
		char& operator[](const size_t loc) {
			return storage[loc];
		}
		const char operator[](const size_t loc)const {
			return storage[loc];
		}
		~string()
		{
			len = 0;
		}
		bool operator==(const string& other)const
		{
			if (len != other.len) return false;
			bool flag = strncmp(other.storage, storage, len);
			if (!flag) return true;
			else return false;
		}
		bool operator!= (const string& other)const
		{
			return !(*this == other);
		}
		bool operator<(const string& other)const
		{
			int del = strncmp(storage, other.storage, len < other.len ? len : other.len);
			if (del == 0)	return len < other.len; 
			if (del < 0)return true;
			else return false;
		}
		friend std::ostream& operator<<(std::ostream &output, const string& out)
		{
			for (size_t i = 0; i < out.len; i++)
				output << out[i];
			/*	if (out.storage != NULL)
			output << out.storage;
			*/
			return output;
		}
		friend std::istream& operator>>(std::istream& input, string& in)
		{
			char s[50];
			input >> s;
			in = s;
			return input;
		}
	};
	int string_to_date(string s)
	{
		int l = s.len;
		int ans = (s[l - 2] - '0') * 10;
		ans += s[l - 1] - '0';
		return ans;
	}
	class time
	{
	public:
		string text;
		int hour, minute;
		time() :text() { hour = 0; minute = 0; }
		time(char*s) :text(s)//promise that the format of s is legal
		{
			//avoid the data like xx:xx
			char c1 = text.storage[0];
			if (c1 >= '0' && c1 <= '9')
			{
				hour = (text.storage[0] - '0') * 10 + (text.storage[1] - '0');
				minute = (text.storage[3] - '0') * 10 + (text.storage[4] - '0');
			}
			else
			{
				hour = 0;
				minute = 0;
			}
		}
		time(const time& other) :text(other.text)
		{
			hour = other.hour;
			minute = other.minute;
		}
		time& operator=(const time& other)
		{
			if (this == &other)return *this;
			text = other.text;
			hour = other.hour;
			minute = other.minute;
			return *this;
		}
		int operator- (const time& other)const
		{
			int ans = (hour - other.hour) * 60 + minute - other.minute;
			return ans;
		}
		friend std::ostream& operator<< (std::ostream& out, const time& t)
		{
			out << t.text;
			return out;
		}
		friend std::istream& operator>> (std::istream& in, time& t)
		{
			char s[50];
			in >> s;
			t = time(s);
			return in;
		}
	};
	class ticket
	{
	public:
		string train_id;
		string loc_from, loc_to;
		char catalog;
		int date_from, date_to;
		time t_from, t_to;
		bool operator==(const ticket& other)
		{
			if (train_id != other.train_id)return false;
			if (loc_from != other.loc_from)return false;
			if (loc_to != other.loc_to)return false;
			if (date_from != other.date_from)return false;
			return true;
		}
		int num;//the num of the ticket kind(scale of seat[])
		class data
		{
		public:
			string ticket_kind;
			int left;//the number of ticket possess or left
			double price;
			data() :ticket_kind() {}
			data(string s, int l, double p) :ticket_kind(s), left(l), price(p) {}
			data& operator=(const data& other)
			{
				if (this == &other)return *this;
				ticket_kind = other.ticket_kind;
				left = other.left;
				price = other.price;
				return *this;
			}
		};
		data seat[15];
		ticket& operator=(const ticket& other){
		    if (this == &other)	return (*this); 
		    train_id = other.train_id; 
		    loc_from = other.loc_from; 
		    loc_to = other.loc_to; 
		    catalog = other.catalog; 
		    date_from = other.date_from; 
		    date_to = other.date_to; 
		    t_from = other.t_from; 
		    t_to = other.t_to; 
		    num = other.num; 
		    for (int i = 0; i < num; i ++)  seat[i] = other.seat[i]; 
		    return (*this); 
		}
		bool operator<(const ticket& other){
		    return train_id < other.train_id; 
		}
		friend std::ostream& operator<< (std::ostream& out, const ticket& tic)
		{
			out << tic.train_id << ' ';
			out << tic.loc_from << ' ' << "2018-06-";
			if (tic.date_from < 10)	out << 0;
			out << tic.date_from << ' ' << tic.t_from << ' ';
			out << tic.loc_to << ' ' << "2018-06-";
			if (tic.date_to < 10)	out << 0;
			out << tic.date_to << ' ' << tic.t_to;
			for (int i = 0; i < tic.num; i++)
			{
				out << ' ' << tic.seat[i].ticket_kind << ' ' << tic.seat[i].left << ' ' << std::setprecision(6) << std::fixed << tic.seat[i].price;
			}
			return out;
		}
	};

	class user
	{
	public:
		static vector<int, 0> num;
		static vector < user > u_store; 
		static map<1, size_t, ticket> tic_store;
		string name, password, email, phone;
		size_t id;
		int privilege;
		int ticket_num;
		user() {}
		user(string n, string p, string e, string ph)
		{
			name = n;
			password = p;
			email = e;
			phone = ph;

			id = num[0] + 2018;
			if (id == 2018)privilege = 2;
			else privilege = 1;
		}
		static size_t newuser(string n, string p, string e, string ph)
		{
			user tmp(n, p, e, ph);
			int v = num[0] + 1;
			num.edit(0, v);
			u_store.push_back(tmp); 
			return tmp.id;
		}

		user(const user& other)
		{
			name = other.name;
			password = other.password;
			email = other.email;
			phone = other.phone;
			id = other.id;
			privilege = other.privilege;
		}
		user& operator=(const user& other)
		{
			if (this == &other)return *this;
			name = other.name;
			password = other.password;
			email = other.email;
			phone = other.phone;
			id = other.id;
			privilege = other.privilege;
			return *this;
		}
		~user()
		{
		}
		bool up_privilege(size_t id2, int pri)//modify id2's privilege to pri
		{
			if (privilege == 1) return false;
			user tmp = u_store[id2 - 2018];
			if (tmp.privilege == 2 && pri == 1)return false;
			tmp.privilege = pri;
			u_store.edit(id2 - 2018, tmp); 
			return true;
		}
		bool login(const string& input)const
		{
			if (password == input)return true;
			else return false;
		}
		void modify(string n, string p, string e, string ph)
		{
			name = n;
			password = p;
			email = e;
			phone = ph;
		}
		void enquiry()
		{
			std::cout << name << ' ' << email << ' ' << phone << ' ' << privilege << '\n';
		}
	};
	vector<int, 0> user::num("num.txt");
	vector < user >  user::u_store("user");
	map<1, size_t, ticket> user::tic_store("ticket");

	class station
	{
	public:
		static vector<string> station_map;
		static map < 0, string, bool > exist_station; 
		string name;
		time arrive, start, stopover;
		int num_price;
		double price[15];
		int sale[15][40];//sale[seat][date] means the left ticket with number"seat" in day"date"
		friend std::ostream& operator<< (std::ostream& out, const station& s)
		{
			string yuan; yuan[0] = -17; yuan[1] = -65; yuan[2] = -91; yuan.len = 3;
			out << s.name << ' ' << s.arrive << ' ' << s.start << ' ' << s.stopover;
			for (int i = 0; i < s.num_price; i++)
				out << ' ' << yuan << s.price[i];
			return out;
		}
	};
	map < 0, string, bool > station::exist_station("station"); 
	vector<string> station::station_map("station");

	class train
	{
	public:
		static map<0, string, train> tr_store;
		static map<1, string, string> world_map;//a function from loc to train_id
		static vector<station> passes;//write the information of every train's station

		string train_id, name;
		char catalog;
		bool sale;
		int num_station, num_price;
		string seat[15];//the name of every seat

		int pass_start, pass_end;//passes[pass_start, pass_end] stores this train's station
		train() {}
		train(string tr_id, string n, char c, int n_s, int n_p) :train_id(tr_id), name(n), catalog(c), num_station(n_s), num_price(n_p)
		{
			sale = false;
		}
		void add_station_and_seat()//maybe anything wrong?
		{
			for (int i = 0; i < num_price; i++)
				std::cin >> seat[i];
			station tmp;
			for (int i = 0; i < num_station; i++)
			{
				std::cin >> tmp.name >> tmp.arrive >> tmp.start >> tmp.stopover;
				char s[100];
				for (int j = 0; j < num_price; j++)
				{
					std::cin >> s;
					tmp.price[j] = price_to_double(s);
				}
				tmp.num_price = num_price;
				//initialize the ticket num
				for (int j = 0; j < num_price; j++)
					for (int k = 1; k <= 30; k++)
					{
						tmp.sale[j][k] = 2000;
					}
				//push them into vector
				if (i == 0)
					pass_start = passes.push_back(tmp);
				else pass_end = passes.push_back(tmp);
			}
		}
		static void new_train(string tr_id, string n, char c, int n_s, int n_p)
		{
			train tmp(tr_id, n, c, n_s, n_p);
			tmp.add_station_and_seat();
			tr_store.insert(tr_id, tmp);
		}
		void query()
		{
			std::cout << train_id << ' ' << name << ' ' << catalog << ' ' << num_station << ' ' << num_price;
			for (int i = 0; i < num_price; i++)
				std::cout << ' ' << seat[i];
			std::cout << '\n';
			for (int i = 0; i < num_station; i++)
			{
				station sta = passes[i + pass_start];
				std::cout << sta << '\n';
			}
		}
		bool del()
		{
			if (sale == true)return false;
			tr_store.erase(train_id);
			return true;
		}
		void modify(string n, char c, int n_s, int n_p)
		{
			name = n;
			catalog = c;
			num_station = n_s;
			num_price = n_p;
			add_station_and_seat();
		}
		bool sale_train()
		{
			if (sale == true)return false;
			string s;//s is each station's name
			for (int i = 0; i < num_station; i++)
			{
				s = passes[i + pass_start].name;
				world_map.insert(s, train_id);
				//when the train is saled, every station passed should be pushed into vector
				if (!station::exist_station.exist(s))
				{
					bool f = true; 
					station::exist_station.insert(s, f); 
					station::station_map.push_back(s);
				}
			}
			sale = true;
			return true;
		}
	};
	map<0, string, train> train::tr_store("train");
	map<1, string, string> train::world_map("world_map");
	vector<station> train::passes("passes");


	/*
	=====================================================
	===the following are functions about each command====
	=====================================================
	*/

	void register_()
	{
		char name[200], password[200], email[200], phone[200];
		std::cin >> name >> password >> email >> phone;
		size_t id = user::newuser(name, password, email, phone);
		std::cout << id << '\n';
	}
	void login()
	{
		size_t id;
		char password[200];
		std::cin >> id >> password;
		//if (!user::u_store.exist(id))
		if (!(user::u_store.size() > id - 2018))
		{
			std::cout << 0 << '\n';
			return;
		}
		string pass(password);
		user tmp = user::u_store[id - 2018];
		bool flag = tmp.login(pass);
		std::cout << flag << '\n';
	}
	void query_profile()
	{
		size_t id;
		std::cin >> id;
		if (!(user::u_store.size() > id - 2018))
		{
			std::cout << 0 << '\n';
			return;
		}
		user tmp = user::u_store[id - 2018];
		tmp.enquiry();
		return;
	}
	void modify_profile()
	{
		size_t id;
		char name[200], password[200], email[200], phone[200];
		std::cin >> id >> name >> password >> email >> phone;
		if (!(user::u_store.size() > id - 2018))
		{
			std::cout << 0 << '\n';
			return;
		}
		user tmp(user::u_store[id - 2018]);
		tmp.name = name;
		tmp.password = password;
		tmp.email = email;
		tmp.phone = phone;
		user::u_store.edit(id - 2018, tmp); 
		std::cout << 1 << '\n';
		return;
	}
	void modify_privilege()
	{
		size_t id1, id2;
		int privilege;
		std::cin >> id1 >> id2 >> privilege;
		if (!(user::u_store.size() > id1 - 2018) || !(user::u_store.size() > id2 - 2018))
		{
			std::cout << 0 << '\n';
			return;
		}
		user tmp = user::u_store[id1 - 2018];
		bool flag = tmp.up_privilege(id2, privilege);
		std::cout << flag << '\n';
	}
	void query_order()//attention!!
	{
		size_t id;
		string odate;
		int date;
		string catalog;
		std::cin >> id >> odate >> catalog;
		date = string_to_date(odate);
		if (!(user::u_store.size() > id - 2018))
		{
			std::cout << -1 << '\n';
			return;
		}
		map<1, size_t, ticket>::iterator start, end, tmp;
		start = user::tic_store.lowerbound(id);
		end = user::tic_store.upperbound(id);
		int success = 0;
		ticket t[100];
		for (tmp = start; tmp != end; tmp++)
		{
			t[success] = *tmp;
			bool flag = false;
			for (size_t i = 0; i < catalog.len; i++)   if (t[success].catalog == catalog[i])    flag = true;
			if (t[success].date_from == date && flag)
				success++;
		}
		std::cout << success << '\n';
		for (int i = 0; i < success; i++)  std::cout << t[i] << '\n';
	}
	void add_train()
	{
		string tr_id, name;
		char catalog;
		int n_s, n_p;
		std::cin >> tr_id >> name >> catalog >> n_s >> n_p;
		if (train::tr_store.exist(tr_id))
		{
			std::cout << 0 << '\n';
			return;
		}
		train::new_train(tr_id, name, catalog, n_s, n_p);
		std::cout << '1' << '\n';
	}
	void sale_train()
	{
		string tr_id;
		std::cin >> tr_id;
		if (!train::tr_store.exist(tr_id))
		{
			std::cout << 0 << '\n';
			return;
		}
		train tmp = train::tr_store[tr_id];
		bool flag = tmp.sale_train();
		if (!flag)std::cout << 0 << '\n';
		else
		{
			train::tr_store.insert(tr_id, tmp);
			std::cout << 1 << '\n';
		}
	}
	void query_train()
	{
		string tr_id;
		std::cin >> tr_id;
		if (!train::tr_store.exist(tr_id))
		{
			std::cout << 0 << '\n';
			return;
		}
		train tmp = train::tr_store[tr_id];
		if (tmp.sale == false)
		{
			std::cout << 0 << '\n';
			return;
		}
		tmp.query();
	}
	void delete_train()
	{
		string tr_id;
		std::cin >> tr_id;
		if (!train::tr_store.exist(tr_id))
		{
			std::cout << 0 << '\n';
			return;
		}
		train tmp = train::tr_store[tr_id];
		if (tmp.sale)
		{
			std::cout << 0 << '\n';
			return;
		}
		train::tr_store.erase(tr_id);
		std::cout << 1 << '\n';
	}
	void modify_train()
	{
		string tr_id, name;
		char c;
		int n_s, n_p;
		std::cin >> tr_id >> name >> c >> n_s >> n_p;
		train tmp1(tr_id, name, c, n_s, n_p);
		tmp1.add_station_and_seat();
		if (!train::tr_store.exist(tr_id))
		{
			std::cout << 0 << '\n';
			return;
		}
		train tmp = train::tr_store[tr_id];
		if (tmp.sale)
		{
			std::cout << 0 << '\n';
			return;
		}
		train::tr_store.insert(tr_id, tmp1);
		std::cout << 1 << '\n';
	}
	void buy_ticket()
	{
		string tr_id, loc1, loc2, ticket_kind, odate;
		int num, date;
		size_t id; 
		std::cin >> id >> num >> tr_id >> loc1 >> loc2 >> odate >> ticket_kind;
		date = string_to_date(odate);
		//if (!train::tr_store.exist(tr_id) || !user::u_store.exist(id))
		if (!train::tr_store.exist(tr_id) || !(user::u_store.size() > id - 2018))
		{
			std::cout << 0 << '\n';
			return;
		}
		train tr_tmp = train::tr_store[tr_id];
		int start = tr_tmp.pass_start, end = tr_tmp.pass_end;
		while (train::passes[start].name != loc1 && start < end)start++;
		while (train::passes[end].name != loc2 && start < end)end--;
		if (end <= start) {
			std::cout << 0 << '\n';
			return;
		}
		int seat = 0;
		for (; seat < tr_tmp.num_price; seat++)
		{
			if (tr_tmp.seat[seat] == ticket_kind)break;
		}
		//seat not found
		if (seat == tr_tmp.num_price)
		{
			std::cout << 0 << '\n';
			return;
		}
		//ticket not enough
		for (int i = start + 1; i <= end; i++)
		{
			if (train::passes[i].sale[seat][date] < num)
			{
				std::cout << 0 << '\n';
				return;
			}
		}
		//modify the num of ticket on the station
		int duration = 0;//days you stay in the train
		station tmp;
		for (int i = start + 1; i <= end; i++)
		{
			tmp = train::passes[i];
			tmp.sale[seat][date] -= num;
			if (train::passes[i].arrive - train::passes[i - 1].start <= 0)  duration++;
			train::passes.edit(i, tmp);
		}
		//creat this ticket
		ticket tic_tmp;
		tic_tmp.date_from = date;
		tic_tmp.date_to = date + duration;
		tic_tmp.loc_from = loc1;
		tic_tmp.loc_to = loc2;
		tic_tmp.t_from = train::passes[start].start;
		tic_tmp.t_to = train::passes[end].arrive;
		tic_tmp.train_id = tr_id;
		tic_tmp.catalog = tr_tmp.catalog;

		//check whether user has already has this kind of ticket(so that the two can merge)
		map<1, size_t, ticket>::iterator l_it, u_it, t_it;
		l_it = user::tic_store.lowerbound(id);
		u_it = user::tic_store.upperbound(id);
		bool flag = false;
		for (t_it = l_it; t_it != u_it; t_it++)
		{
			if (*t_it == tic_tmp)
			{
				ticket t = *t_it;
				t.seat[seat].left += num;
				t_it.edit(t);
				flag = true;
			}
		}
		if (!flag)//in this case, store a new ticket
		{
			//need to copy all kinds of seat(even if only buy one)
			tic_tmp.num = tr_tmp.num_price;
			for (int i = 0; i < tic_tmp.num; i++)
			{
				tic_tmp.seat[i].ticket_kind = tr_tmp.seat[i];
				tic_tmp.seat[i].left = 0;
				tic_tmp.seat[i].price = 0;
				for (int j = start + 1; j <= end; j++)	tic_tmp.seat[i].price += train::passes[j].price[i];
			}
			//buy the ticket
			tic_tmp.seat[seat].left = num;
			user::tic_store.insert(id, tic_tmp);
			user u_tmp = user::u_store[id = 2018];
			u_tmp.ticket_num++;
			user::u_store.edit(id - 2018, u_tmp); 
		}
		std::cout << 1 << '\n';
		return;
	}
	void refund_ticket()
	{
		size_t id;
		int num, date;
		string tr_id, loc1, loc2, odate, kind;
		std::cin >> id >> num >> tr_id >> loc1 >> loc2 >> odate >> kind;
		if (!train::tr_store.exist(tr_id) || !(user::u_store.size() > id - 2018))
		{
			std::cout << 0 << '\n';
			return;
		}
		date = string_to_date(odate);
		ticket t_tmp;
		t_tmp.train_id = tr_id;
		t_tmp.loc_from = loc1;
		t_tmp.loc_to = loc2;
		t_tmp.date_from = date;
		user u_tmp = user::u_store[id = 2018];
		bool flag = false;
		int seat = 0;
		map<1, size_t, ticket>::iterator u_it, l_it, t_it;
		l_it = user::tic_store.lowerbound(id);
		u_it = user::tic_store.upperbound(id);
		for (t_it = l_it; t_it != u_it; t_it++)
		{
			if (*t_it == t_tmp)
			{
				ticket cmp = *t_it;
				for (; cmp.seat[seat].ticket_kind != kind; seat++);
				if (seat < cmp.num && cmp.seat[seat].left >= num)
				{
					cmp.seat[seat].left -= num;
					t_it.edit(cmp);
					flag = true;
					break;
				}
			}
		}
		if (!flag)//not enough ticket, or not found
		{
			std::cout << 0 << '\n';
			return;
		}
		//user::u_store.insert(id, u_tmp);
		user::u_store.edit(id - 2018, u_tmp); 
		train tr_tmp = train::tr_store[tr_id];
		int start = tr_tmp.pass_start, end = tr_tmp.pass_end;
		while (train::passes[start].name != loc1)start++;
		while (train::passes[end].name != loc2)end--;
		//modify ticket num in the station
		station tmp;
		for (int i = start; i <= end; i++)
		{
			tmp = train::passes[i];
			tmp.sale[seat][date] += num;
			train::passes.edit(i, tmp);
		}
		std::cout << 1 << '\n';
		train::tr_store.insert(tr_id, tr_tmp);
		return;
	}
	string* query_ticket(string loc1, string loc2, int& num)
	{
		string* ans = new string[400];
		num = 0;
		map<1, string, string>::iterator s1, e1, s2, e2, i, j;
		s1 = train::world_map.lowerbound(loc1);
		e1 = train::world_map.upperbound(loc1);
		s2 = train::world_map.lowerbound(loc2);
		e2 = train::world_map.upperbound(loc2);
		map<0, string, bool> tmp("tmp"); 
		tmp.clear(); 
		for (i = s1; i != e1; i ++) tmp.insert(*i, true); 
		for (i = s2; i != e2; i ++) 
		    if (tmp.exist(*i)){
		    	ans[num ++] = *i; 
		    }
		return ans;
	}
	void query_ticket()
	{
		string loc1, loc2, datestring;
		string catalog;
		int num, date;
		std::cin >> loc1 >> loc2 >> datestring >> catalog;
		date = string_to_date(datestring);
		//the train_id found only means this train passes the two location
		//I have to check whether there's ticket left between loc1 and loc2
		string* dest = query_ticket(loc1, loc2, num);
		train tmp;
		int start, end, success = 0;
		ticket out[200];
		for (int k = 0; k < num; k++)//for every train_id
		{
			tmp = train::tr_store[dest[k]];
			bool flag = false;
			for (size_t i = 0; i < catalog.len; i++)   if (tmp.catalog == catalog[i])	flag = true;
			if (!flag)  continue;
			start = tmp.pass_start;
			end = tmp.pass_end;
			for (; start <= end && train::passes[start].name != loc1; start++);
			for (; end >= start && train::passes[end].name != loc2; end--);
			if (start >= end)    continue;
			int duration = 0;//
			int min_seat[15] = { 0 };//for every seat, find the min ticket left
			for (int i = 0; i < tmp.num_price; i++)
				min_seat[i] = 2000;
			double price[15] = { 0 };
			for (int i = start + 1; i <= end; i++)
			{
				if (train::passes[i].arrive - train::passes[i - 1].start <= 0)
					duration++;
				for (int j = 0; j < tmp.num_price; j++)
				{
					price[j] += train::passes[i].price[j];
					if (min_seat[j] > train::passes[i].sale[j][date])
						min_seat[j] = train::passes[i].sale[j][date];
				}
			}
			flag = true;
			for (int i = 0; i < tmp.num_price; i++)
				if (min_seat[i] == 0)
				{
					flag = false;
					break;
				}
			if (!flag)continue;
			out[success].loc_from = loc1;
			out[success].loc_to = loc2;
			out[success].date_from = date;
			out[success].date_to = date + duration;
			out[success].t_from = train::passes[start].start;
			out[success].t_to = train::passes[end].arrive;
			for (int j = 0; j < tmp.num_price; j++)
			{
				out[success].seat[j].ticket_kind = tmp.seat[j];
				out[success].seat[j].left = min_seat[j];
				out[success].seat[j].price = price[j];
			}
			out[success].train_id = tmp.train_id;
			out[success].catalog = tmp.catalog;
			out[success].num = tmp.num_price;
			success++;
		}
		std::cout << success << '\n';
		for (int i = 0; i < success; i++)
			std::cout << out[i] << '\n';
		delete []dest;
	}
	int cal_time(string catalog, string tr_id, string loc1, string loc2, time& t_start, time& t_end)//calculate the time train_id uses from loc1 to loc2 (minute)
	{
		train tr = train::tr_store[tr_id];
		bool flag = false;
		for (size_t i = 0; i < catalog.len; i++)
		{
			if (tr.catalog == catalog[i])
			{
				flag = true;
				break;
			}
		}
		if (!flag)
		{
			return -1;
		}
		station sta;
		int start = tr.pass_start, end = tr.pass_end;
		for (; train::passes[start].name != loc1; start++);
		for (; train::passes[end].name != loc2; end--);
		t_start = train::passes[start].start;
		t_end = train::passes[end].arrive;
		int ans = 0;
		int duration = 0;
		for (int i = start + 1; i <= end; i++)
		{
			if (train::passes[i].arrive - train::passes[i - 1].start <= 0)
				duration++;
		}
		ans += duration * 24 * 60;
		ans += (train::passes[end].arrive - train::passes[start].start);
		return ans;
	}
	void query_transfer()
	{
		string loc1, loc2, datestring, catalog;
		std::cin >> loc1 >> loc2 >> datestring >> catalog;
		int date = string_to_date(datestring);
		string mids;//the transfer station
		int min_time = 1000000000;
		string min_mids, min_tr1, min_tr2;
		string* m1, *m2;
		//for every mid_transfer station, use two vector to store every train's start-arrive time and duration
		struct data
		{
			int duration;
			time start;
			time end;
		};
		data m1_s[100], m2_s[100];
		int num1, num2;//the scale of the two vector
		for (size_t i = 0; i < station::station_map.size(); i++)
		{
			mids = station::station_map[i];
			if (mids == loc1 || mids == loc2)continue;
			m1 = query_ticket(loc1, mids, num1);
			m2 = query_ticket(mids, loc2, num2);
			for (int i = 0; i < num1; i++)
			{
				m1_s[i].duration = cal_time(catalog, m1[i], loc1, mids, m1_s[i].start, m1_s[i].end);
			}
			for (int i = 0; i < num2; i++)
			{
				m2_s[i].duration = cal_time(catalog, m2[i], mids, loc2, m2_s[i].start, m2_s[i].end);
			}
			int tmp = 0;
			for (int i = 0; i < num1; i++)
			{
				if (m1_s[i].duration < 0)continue;
				for (int j = 0; j < num2; j++)
				{
					if (m2_s[j].duration < 0)continue;
					tmp = m1_s[i].duration + m2_s[j].duration;
					tmp += (m2_s[j].start - m1_s[i].end);
					if (m2_s[j].start - m1_s[i].end < 0)tmp += 24 * 60;//wait for the next train over 0:00
					if (min_time > tmp)
					{
						tmp = min_time;
						min_mids = mids;
						min_tr1 = m1[i];
						min_tr2 = m2[j];
					}
				}
			}
			delete []m1;
			delete []m2;
		}
		if (min_tr1.len == 0){
			std::cout << -1 << '\n'; 
			return; 
		}
		//the min transfer already found
		//calculate ticket and output
		//attention: there's one bug: if the min transfer has no ticket in some of the station, I can't pick it out.
		//however I guess the test data won't do that cruel thing.
		int start, end;
		train tr_tmp;
		string los, loe;
		ticket out;
		for (int i = 0; i <= 1; i++)
		{
			if (i == 0)
			{
				tr_tmp = train::tr_store[min_tr1];
				los = loc1;
				loe = min_mids;
			}
			else
			{
				tr_tmp = train::tr_store[min_tr2];
				los = min_mids;
				loe = loc2;
			}
			start = tr_tmp.pass_start;
			end = tr_tmp.pass_end;
			for (; start <= end && train::passes[start].name != los; start++);
			for (; end >= start && train::passes[end].name != loe; end--);
			int duration = 0;//
			int min_seat[15] = { 0 };//for every seat, find the min ticket left
			for (int i = 0; i < tr_tmp.num_price; i++)
				min_seat[i] = 2000;
			double price[15] = { 0 };
			for (int i = start + 1; i <= end; i++)
			{
				if (train::passes[i].arrive - train::passes[i - 1].start <= 0)
					duration++;
				for (int j = 0; j < tr_tmp.num_price; j++)
				{
					price[j] += train::passes[i].price[j];
					if (min_seat[j] > train::passes[i].sale[j][date])
						min_seat[j] = train::passes[i].sale[j][date];
				}
			}
			out.loc_from = los;
			out.loc_to = loe;
			out.date_from = date;
			out.date_to = date + duration;
			out.t_from = train::passes[start].start;
			out.t_to = train::passes[end].arrive;
			for (int j = 0; j < tr_tmp.num_price; j++)
			{
				out.seat[j].ticket_kind = tr_tmp.seat[j];
				out.seat[j].left = min_seat[j];
				out.seat[j].price = price[j];
			}
			out.train_id = tr_tmp.train_id;
			out.catalog = tr_tmp.catalog;
			out.num = tr_tmp.num_price;
			std::cout << out << '\n';
		}
	}
	void clean()
	{
		user::u_store.clear();
		user::tic_store.clear();
		train::tr_store.clear();
		train::world_map.clear();
		station::exist_station.clear(); 
		user::num.clear(); 
		station::station_map.clear(); 
		train::passes.clear(); 
		std::cout << 1 << '\n';
	}
	void exit()
	{
		std::cout << "BYE" << '\n';
		//exit();
	}
}
